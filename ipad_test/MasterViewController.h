//
//  MasterViewController.h
//  ipad_test
//
//  Created by Javier Cuesta Gómez on 25.04.14.
//  Copyright (c) 2014 Javier Cuesta Gómez. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DetailViewController;

@interface MasterViewController : UITableViewController

@property (strong, nonatomic) DetailViewController *detailViewController;

@end
