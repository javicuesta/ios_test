//
//  DetailViewController.h
//  ipad_test
//
//  Created by Javier Cuesta Gómez on 25.04.14.
//  Copyright (c) 2014 Javier Cuesta Gómez. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailViewController : UIViewController <UISplitViewControllerDelegate>

@property (strong, nonatomic) id detailItem;

@property (weak, nonatomic) IBOutlet UILabel *detailDescriptionLabel;
@end
